Build the image by running nix-build in this folder.
Then load, tag and push it to the gitlab registry as documented [here](https://gitlab.com/help/user/project/container_registry).
