Bisher in Mathe-Maister erwähnte Mathematiker, sortiert nach Nachnamen (Lebenszeit und Nationalität nach Wikipedia):


	A:
<b>Leonidas Alaoglu</b> (🇨🇦 1914-1981)
<b>Archimedes</b> von Syrakus (🇬🇷 ca 287-212 BC)

	B:
<b>Stefan Banach</b> (🇵🇱 1892-1945)
<b>Jakob I Bernoulli</b> (🇨🇭 1655-1705)
<b>Pierre Ossian Bonnet</b> (🇫🇷 1819-1892)
Félix Édouard Justin <b>Émile Borel</b> (🇫🇷 1871-1956)
<b>Luitzen</b> Egbertus Jan <b>Brouwer</b> (🇳🇱 1881-1966)

	C:
<b>Georg</b> Ferdinand Ludwig Philipp <b>Cantor</b> (🇩🇪 1845-1918)
<b>Augustin-Louis Cauchy</b> (🇫🇷 1789-1857)
<b>Bonaventura</b> Francesco <b>Cavalieri</b> (🇮🇹 1598-1647)
<b>Alexis-Claude Clairaut</b> (🇫🇷 1713-1765)
<b>André-Louis Cholesky</b> (🇫🇷 1875-1918)
<b>Gabriel Cramer</b> (🇨🇭 1704-1752)

	E:
<b>Euklid</b> von Alexandria (🇬🇷 ca 300-200 BC)
<b>Leonhard Euler</b> (🇨🇭 1707-1783)

	F:
<b>Pierre</b> Joseph Louis <b>Fatou</b> (🇫🇷 1878-1929)
<b>Guido Fubini</b> (🇮🇹 1879-1943)
<b>Jean Frédéric Frenet</b> (🇫🇷 1816-1900)
<b>Ferdinand Georg Frobenius</b> (🇩🇪 1849-1917)

	G:
Johann <b>Carl Friedrich Gauss</b> (🇩🇪 1777-1855)
<b>Édouard</b> Jean-Baptiste <b>Goursat</b> (🇫🇷 1858-1936)
<b>Jørgen Pedersen Gram</b> (🇩🇰 1850-1916)
<b>George Green</b> (🇬🇧 1793-1841)

	H:
<b>Felix Hausdorff</b> (🇩🇪 1868-1942)
Heinrich <b>Eduard Heine</b> (🇩🇪 1821-1881)
<b>David Hilbert</b> (🇩🇪 1862-1943)
<b>Heinz Hopf</b> (🇩🇪/🇨🇭 1894-1971)
<b>Alston Scott Householder</b> (🇺🇸 1904-1993)

	J:
<b>Carl Gustav Jacob Jacobi</b> (🇩🇪 1804-1851)
Marie Ennemond <b>Camille Jordan</b> (🇫🇷 1838-1922)

	K:
<b>Leopold Kronecker</b> (🇩🇪 1823-1891)

	L:
<b>Joseph-Louis</b> de <b>Lagrange</b> (🇮🇹 1736-1813)
<b>Pierre-Simon</b> (Marquis de) <b>Laplace</b> (🇫🇷 1749-1827)
<b>Henri</b> Léon <b>Lebesgue</b> (🇫🇷 1875-1941)
<b>Gottfried Wilhelm Leibniz</b> (🇩🇪 1646-1716)
<b>Beppo Levi</b> (🇮🇹 1875-1961)
<b>Joseph Liouville</b> (🇫🇷 1809-1882)

	M:
<b>Giacinto Morera</b> (🇮🇹 1856-1909)
<b>Augustus De Morgan</b> (🇬🇧 1806-1871)

	N:
<b>Isaac Newton</b> (🇬🇧 1643-1727)

	P:
<b>Giuseppe Peano</b> (🇮🇹 1858-1932)
Jules <b>Henri Poincaré</b> (🇫🇷 1854-1912)

	R:
Georg Friedrich <b>Bernhard Riemann</b> (🇩🇪 1826-1866)
<b>Frigyes Riesz</b> (🇭🇺 1880-1956)

	S:
<b>Arthur Sard</b> (🇺🇸 1909-1980)
<b>Pierre Frédéric Sarrus</b> (🇷🇺 1798-1861)
<b>Erhard Schmidt</b> (🇩🇪 1876-1959)
<b>Hermann Amandus Schwarz</b> (🇩🇪 1843-1921)
<b>Philipp Ludwig</b> Ritter <b>von Seidel</b> (🇩🇪 1821-1896)

	T:
<b>Andrei Nikolajewitsch Tichonow</b> (🇷🇺 1906-1993)
<b>Leonida Tonelli</b> (🇮🇹 1885-1946)

	W:
<b>Hassler Whitney</b> (🇺🇸 1907-1989)


Flaggen:
Dänemark 🇩🇰
Deutschland 🇩🇪
England (/GB) 🇬🇧
Frankreich 🇫🇷
Griechenland 🇬🇷
Italien 🇮🇹
Kanada 🇨🇦
Niederlande 🇳🇱
Polen 🇵🇱
Russland 🇷🇺
Schweiz 🇨🇭
Ungarn 🇭🇺
USA 🇺🇸


fehlt: ...
