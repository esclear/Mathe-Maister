# Mathe-Maister
Ansammlung einiger wichtiger themenübergreifender mathematischer Definitionen und Sätze, später dann verpackt in Anki in schönem Latex, um die Sprache der Mathematik fließender zu verstehen.


## Aktuelle Version:
**1.2.0**  
Vergangene und geplante Änderungen einsehbar in den [Patch Notes](Anleitungen-Links-etc/Patch-Notes.md)  
Bekannte Fehler nach Updates gibt es bei den [Kompatibilitätsproblemen](#kompatibilit%C3%A4tsprobleme)


# FAQ
### > Wie komme ich in den Genuss der Mathe-Maister Decks?
- **Anki runterladen** (zum Beispiel AnkiDroid im Play Store). Damit hat man die
  Software, die mit den erstellten Karten etwas anfangen kann. Das Programm ist
  anfangs komplet leer, da Anki selbstverständlich nicht weiß, was man lernen
  will!
- Gewünschte **Decks runterladen und importieren** (siehe unten). Alle mathematischen Inhalte,
  meine Designentscheidungen zum Lernerlebnis, und LaTeX-Support (braucht
  bedingt Internet, siehe unten), sind schon enthalten.
- einfach **loslernen und täglich dranbleiben**


### > Wo kann ich Anki runterladen?
* für den PC: https://apps.ankiweb.net/
* für Android: ***AnkiDroid*** auf
  **[F-Droid](https://f-droid.org/packages/com.ichi2.anki/)** oder im **[Play
  Store](https://play.google.com/store/apps/details?id=com.ichi2.anki)**
* für iOS: nicht sicher, aber sollte ***AnkiMobile*** heißen. Kostet aber auch was. ACHTUNG: Bisher wurden keine Tests auf iOS Anki durchgeführt!


### > Wie bleibe ich auf dem Laufenden zu großen Updates?
Dafür gibt es [*diesen E-Mail-Verteiler*](http://supersecret.me/ankiprojekt/), auf dem man sich jederzeit ein- und austragen kann.


## > Wo finde ich die Anki-Decks?
Hier:  

Laufende:
+ [**Ana LA Grundlagen** (fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.apkg)
+ [**Analysis I** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.apkg)
+ [**Topologie** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.apkg)
+ [**Lineare Algebra II** (startet demnächst)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-II.apkg)
+ [**Funktionentheorie** (läuft))](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.apkg)
+ [**Seminar Geometrie von Flächen** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flächen.apkg)
+ [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.apkg)

Pausiert, bald angefangen oder lange fertig:
+ [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.apkg)
+ [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.apkg)
+ [**Analysis III** (pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.apkg)
+ [**Geometrie I** (pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.apkg)
+ [**Seminar zu Differentialformen** (bis auf weiteres pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.apkg)
+ [**Funktionalanalysis** (abgebrochen)](https://cptmaister.gitlab.io/Mathe-Maister/Funktionalanalysis.apkg)
+ [**Optimierung I** (leer, gibts hoffentlich bald)](https://cptmaister.gitlab.io/Mathe-Maister/Optimierung-I.apkg)


## > Wo kann ich mir die Inhalte der Karteikarten auf einem Blick ansehen?
[Das Inhaltsverzeichnis befindet sich hinter diesem Link](Anleitungen-Links-etc/Inhaltsverzeichnis.md). Darin werden die Inhalte aller Decks möglichst treffend beschrieben, mit Inhaltsverzeichnis für die jeweiligen Decks.  

Hier sind die Links zu für Menschen schön lesbaren PDFs aller laufenden Stoffsammlungen. Inhaltliche Korrektheit ohne Gewähr, vollständige Sammlungen teilweise durch Promovierende o.ä. korrigiert:

Laufende:
+ [**Ana LA Grundlagen** (fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.pdf)
+ [**Analysis I** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.pdf)
+ [**Topologie** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.pdf)
+ [**Lineare Algebra II** (startet demnächst)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-II.pdf)
+ [**Funktionentheorie** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.pdf)
+ [**Seminar Geometrie von Flächen** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flächen.pdf)
+ [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.pdf)

Pausiert, bald angefangen oder lange fertig:
+ [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.pdf)
+ [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.pdf)
+ [**Analysis III** (pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.pdf)
+ [**Geometrie I** (pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.pdf)
+ [**Seminar zu Differentialformen** (bis auf weiteres pausiert)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.pdf)
+ [**Funktionalanalysis** (abgebrochen)](https://cptmaister.gitlab.io/Mathe-Maister/Funktionalanalysis.pdf)
+ [**Optimierung I** (leer, gibts hoffentlich bald)](https://cptmaister.gitlab.io/Mathe-Maister/Optimierung-I.pdf)


## Motivation
Als Hobbyprojekt einiger weniger Studenten der Uni Augsburg entstehen momentan Karteikarten zu Definitionen und Sätzen aus vielen Vorlesungen. Diese sollen jedem Studenten ermöglichen, Erinnerung zur Entscheidung zu machen und sich im Studienalltag seltener mit Nachschlagen von schon wieder vergessenem beschäftigen zu müssen.

Um schon verstandenes oder stur auswendiglernbares effizient ins Langzeitgedächtnis zu bekommen, hat sich das Spaced Repetition System als besonders nützlich herausgestellt, d.h. man beschäftigt sich zwar regelmäßig mit den einzelnen Stofffetzen (z.B. Definitionen oder Vokabeln), aber jeweils nach zunehmend länger werdenden Intervallen (1 Tag, 4 Tage, 2 Wochen, ...). Die Zeitintervalle zwischen einzelnen Erinnerungen soll dabei stärker zunehmen, je besser man es noch in Erinnerung hatte.

Karteikarten eignen sich dafür sehr gut, aber klassische Papierkarteikarten nur bedingt:
 - nach kurzer Zeit hat man zu viele, um sie immer bequem dabei zu haben
 - es ist schwer, sie mit Freunden zu teilen oder gemeinsam zu bearbeiten
 - die Sortierung und Einordnung für die Lernsessions der nächsten Tage wird schnell unübersichtlich

Die Open Source App Anki (für Android: AnkiDroid) löst diese Probleme und bringt viele andere Vorteile mit sich. Anki ist dabei nur unsere schlaue Karteikartenbox: Karten zu verschiedensten Themengebieten können einfach angefertigt und unter anderem online geteilt werden. Die Erstellung ausführlicher, aber vor allem knackiger und präziser Karten zum Mathestudium ist das Ziel des Projekts Mathe-Maister.  

Diese Decks sind aber explizit nicht dafür geeignet, Konzepte neu zu erlernen, sondern sollten nur dafür verwendet werden, schon gelernte Konzepte nicht zu vergessen, oder neue Definitionen schnell zu verinnerlichen. Dafür wird man, durch Einsatz von Anki mit diesen Decks, passend an genaue Formulierungen erinnert und muss dann, nach eigenem Ermessen, diese streng genug wiedergeben.  

*Extra*: [Interaktiver Webcomic zum Spaced Repetition System](https://ncase.me/remember/)  


### > Wie funktioniert Anki, was kann ich da alles machen?
Wirklich wichtige Funktionen für dieses Projekt beschreibe ich im Folgenden, den
Rest kann man sich selbst aneignen durch Rumprobieren, auf
https://apps.ankiweb.net/docs/manual.html Nachschauen, Video-Tutorials
Anschauen, Blog Posts dazu Lesen, mich Fragen, ...  


### > Wie kann ich ein runtergeladenes Deck in Anki reinstecken/importieren?
- Am PC genügt es normalerweise, nachdem Anki installiert wurde, einfach auf die
  Datei vom Deck  (sollte als Endung `.apkg` haben)  doppelt zu klicken. Alternativ per
  Drag&Drop in Anki reinziehen. Oder Anki  öffnen und in der Menüleiste auf File
  -> Import... und da dann das Deck auswählen und öffnen. Download eines Decks über Microsoft Edge führt oft zum Fehler, dass die Dateiendung geändert wird. Um dieses Problem zu lösen, einfach die Dateiendung zu `.apkg` ändern oder einfach [einen gescheiten Browser](https://www.mozilla.org/en-US/firefox/new/) nutzen.  
- Am Handy reicht es normalerweise auch, die Datei runterzuladen und dann zu
  öffnen. Bei mir zumindest wird es dann ohne Probleme von Anki erkannt und
  importiert. Ich empfehle aber, alle Decks am PC zu importieren und dann durch
  Synchronisierung aufs Handy zu bekommen. Ist aber reine Geschmackssache.  


### > Meine Karten werden nicht richtig angezeigt, ich sehe nur unleserliche Zeichen und Brabbeleien
Auf dem Handy einfach kurz Internet anmachen, Deck nochmal neu öffnen, dann sollte
es funktionieren bis zum kompletten Schließen der App. Danach braucht man wieder
kurz Internet, damit der Code kurz laden kann. Auf Windows PCs funktioniert 
es bisher nur manchmal, kann aber manchmal behoben werden durch Download dieses 
[Add-Ons](https://ankiweb.net/shared/info/1280253613). Auf Linuxmint funktioniert es.


### > Wie füge ich neu veröffentlichte oder neuere Versionen von Karten zu meinem bestehenden Deck hinzu?
Einfach die neuste Version runterladen und öffnen/importieren. Es sollten alle 
Karten im alten Deck automatisch aktualisiert werden, die geändert wurden, und 
neue Karten hinzugefügt werden. Der Lernfortschritt geht dabei nicht verloren.  
Bitte eine Nachricht an mich, falls das mal nicht klappt, wir sind da noch am 
Rumprobieren!


## > Wie kann ich ein eigenes Deck erstellen oder ein bestehendes Deck erweitern?
Hier möchte ich nur den einfachsten Weg erklären. Kompliziertere Wege würden so viel Vorwissen verlangen, dass man es auch selbst rausfinden kann. Irgendwann bald gibt es hoffentlich auch Videos dazu, aber vorerst der einfachste Weg:

Beitragen zu einem Deck kann man ganz einfach, indem man im Ordner `Stoffsammlung` die entsprechende `CSV`-Datei runterlädt, diese bearbeitet und mir dann zuschickt per Mail oder, besser noch, per Merge Request hier auf gitlab.  

Welche Eigenheiten beim Erstellen von Inhalten zu beachten sind, kann man in den [Hinweisen zur Stoffsammlung](Anleitungen-Links-etc/Hinweise-Stoffsammlung.md) einsehen.  

Selbst ein Deck erstellen geht ähnlich einfach. Pro Deck muss jeweils ein Ordner für Bilder, eine `CSV`-Datei, eine `JSON`-Datei (Konfigurationsdatei) existieren. Gerne kann man sich einfach nur um die CSV-Datei kümmern, sie mir zuschicken, und ich mache das mit der Konfigurationsdatei und lege den Bilderordner an, in dem fortan alle anderen Bilder auch gelagert werden können. Die Konfigurationsdatei muss nur am Anfang erstellt werden und enthält als einzig interessante, später eventuell änderwerte Information die in Anki sichtbare Beschreibung und den Decknamen.  
Wichtig ist noch, dass der Name der neuen CSV-Datei nicht mit dem Namen eines schon existierenden Decks übereinstimmt. Da der Name jederzeit geändert werden kann, ohne Auswirkungen für Nutzer der Decks zu haben, soll das zunächst mal kein Problem darstellen, bis wir uns ein cleveres System zur Benennung überlegt haben (sobald dann die ersten verschiedenen Decks zu den gleichen Vorlesungen entstehen).  


### Karten Aktualisieren und Hinzufügen durch CSV-Import
**!!! Nur notwendig für Updates von Decks von vor Version 1 !!!**
* `CSV`-Datei zur passenden Vorlesung in der [Stoffsammlung](Stoffsammlung) runterladen
* in Anki auf das zu aktualisierende Deck klicken, dann oben links *File* → *Import...* und die Datei auf eurem Computer öffnen
* ein neues Fenster ist in Anki offen, hier auf ein paar wichtige Sachen achten:
	* Ganz oben links bei `Type` den Notiztyp *Mathe-Maister Definitionen* (könnte leicht abweichen) wählen
	* Daneben bei `Deck` das Deck, in dem ihr die Karten aktualisieren oder hinzufügen wollt
	* Gleich darunter `Fields seperated by: Tab` auswählen
	* Direkt darunter `Update existing notes when first field matches` wählen 
	* Direkt darunter Das Häkchen ☑ setzen bei `Allow HTML in fields`
	* Alles hierunter, also die Feldzuweisungen, sollten stimmen
	* ganz unten auf *Import* klicken
* in einem Ergebnisfenster seht ihr, wie viele Karten hinzugefügt und verändert wurden.  
Achtung: das erste Feld jeder Notiz muss genau übereinstimmen mit dem ersten Eintrag der Zeile, sonst entsteht eine neue Notiz, statt einem Update. Dafür nach dem Import einfach schauen, welche neue Notizen entstanden sind, entsprechende alte Notizen schnell anpassen, neu entstandene Karten wieder löschen, nochmal CSV-Import durchführen. Das ganze so oft durchführen, bis keine neue Karten mehr entstehen (nach 1 bis 2 mal hat man normalerweise alles erwischt).


### > Wie bleibe ich motiviert? / Wie kann ich die Lernerfahrung für mein leicht ablenkbares Gehirn angenehmer gestalten?
 + In den Optionen ausstellen, dass man während dem Lernen sieht, wie viele Karten noch übrig sind (lenkt sonst hart ab!)
 + [Puppy Reinforcement](https://ankiweb.net/shared/info/1722658993), ein Add-On, das einem ungefähr alle 10 Karten ein süßes Bild von einem Welpen zeigt. Enthält 50 Bilder, kann man aber nach Belieben erweitern und ersetzen. Häufigkeit auch umstellbar (Add-Ons sind generell nicht für mobile Anki-Versionen verfügbar, aber nicht vergessen: auf allen von mir bisher getesteten Linux Rechnern funktioniert das ganze Latex-Gerendere, also kann man auf einem Linux-Rechner genau so gut lernen, wie auf dem Handy, nur jetzt mit Hundewelpen!)
 + [Review Heatmap](https://ankiweb.net/shared/info/1771074083), ein Add-On, mit dem man zur gesamten eigenen Anki-Sammlung und zu einzelnen eigenen Decks die bisherigen und voraussichtlichen Wiederholungen übersichtlich betrachten kann. Sehr motivierend, da keine Lücke reinzubekommen!
 + [Progress Bar](https://ankiweb.net/shared/info/2091361802), *a progress bar that shows your progress in terms of passed cards per review session.*
 + [Life Drain](https://ankiweb.net/shared/info/715575551), *this add-on adds a life bar during your reviews. Your life reduces within time, and you must answer the questions in order to recover it.* Schön motivierend und auch so einstellbar, dass man kein Leben zurückgewinnt. Damit kann man sich ganz schön einen Timer einstellen, zum Beispiel fürs Lernen nach der [Pomodoro-Technik](https://de.wikipedia.org/wiki/Pomodoro-Technik).
 + [Night Mode für am Rechner](https://ankiweb.net/shared/info/1496166067).
 + ...mehr gibts hier nicht. Schick mir deine Lieblingstricks und -Add-Ons!


### > Hast du was zum Prokrastinieren für mich da?
Hier sind ein paar cool aussehende Decks für zwischendurch (habe ich größtenteils selbst noch nicht getestet):
[Ultimate Geography](https://ankiweb.net/shared/info/2109889812), 
[Countries of the World](https://ankiweb.net/shared/info/2915332392), 
[100 Colors](https://ankiweb.net/shared/info/2033234340), 
[Great Works of Art](https://ankiweb.net/shared/info/685421036), 
[Piano Basics](https://ankiweb.net/shared/info/347130449), 
[Vogelstimmen](https://ankiweb.net/shared/info/2088996377), 
[NATO phonetic alphabet](https://ankiweb.net/shared/info/766972333), 
[Star Constellations](https://ankiweb.net/shared/info/1876097095) (schaut schick aus, aber demnächst kommt sogar ein geiles Sternbilder Deck von Samuel und mir!), 
[Fonts characteristics and history](https://ankiweb.net/shared/info/1770056221), 
[Python code quiz](https://ankiweb.net/shared/info/51975584),
[Japanese Hiragana](https://ankiweb.net/shared/info/2183294427), 
[Katakana Reading Practice](https://ankiweb.net/shared/info/2015522924), 
[5000 most common French words](https://ankiweb.net/shared/info/468622242), 
[Periodic table mnemomincs](https://ankiweb.net/shared/info/490209917), 
[Knots and how to tie them](https://www.memrise.com/course/649284/knots-and-how-to-tie-them/) (ist zwar auf Memrise, aber [es gibt Anki Add-Ons](https://github.com/wilddom/memrise2anki-extension), die ein Memrise Deck komplett herunterladen und zu Anki hinzufügen können).


## Kompatibilitätsprobleme
**Achtung!!** Decks von Version 1.0.2 - 1.1.0 sind nicht automatisch kompatibel mit den aktuellen Decks. Das lässt sich aber ganz leicht beheben. Wie die Fehlermeldung "Aktualisierung ignoriert da der Notiztyp verändert wurde" nahelegt, wurde jedem Deck ein Feld für Bilder hinzugefügt. Deswegen einfach vor dem Import einer neueren Deckversion dem Notiztyp, der im Deck für die Karten verwendet wird, ein neues Feld mit dem Namen "Illustration" hinzufügen. Bei Problemen einfach mich ansprechen.  
**Mini-Achtung!** Sorry falls du dir ein Deck von V1.0.0 - V1.0.2 runtergeladen hast, sind leider nicht mehr kompatibel mit zukünftigen Decks wegen einem Skriptfix! (kommt hoffentlich nicht mehr vor)  
**Achtung!** Vor Version 1.0.0 verfügbare Decks sind nicht mehr kompatibel mit der automatischen Aktualisierung durch den Anki-apkg-Import (Standard Deckimport durch Öffnen der Datei etc) der neuen Decks seit Version 1.0.0.  
Falls du ein Deck von vor Version 1.0.0 aktualisieren möchtest (also nicht über einen der Links hier drunter, sondern aus einem Ordner hier drüber runtergeladen), dann verwende den [unten Beschriebenen CSV-Import](#karten-aktualisieren-und-hinzuf%C3%BCgen-durch-csv-import).  


## To Do
- [x] Projekt starten
- [ ] Co-Autoren finden *sheesh*
- [x] Skripte zur Automatisierung: CSV zu TEX zu PDF, CSV zu APKG (mit Bildern), Gitlab CI
- [x] Nichts generiertes im Repo ablegen: PDFs, Anki Decks
- [ ] Vorlesungen
	- [ ] Grundlagenvorlesungen
        - [ ] Ana LA Grundlagen
            - [x] vorerst fertig
            - [ ] Korrektur
		- [x] LA 1: fertig (WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte, Korrektur: Ingo)
		- [ ] **LA 2**: fängt demnächst an
		- [ ] **Ana 1**: läuft
		- [ ] Ana 2: ab Oktober?
		- [ ] Algebra 1: ab Oktober
		- [ ] Geo 1: pausiert
		- [ ] Opti 1: vieleicht bald
		- [x] Num 1: fertig (WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines, Korrektur: Roland)
		- [ ] Stochastik 1 ???
	- [ ] Weiterführende Vorlesungen
		- [ ] Ana 3: pausiert
		- [ ] Algebra 2: ab April 2020?
		- [ ] **Topologie**: läuft heiß
		- [ ] **Funktionentheorie**: läuft
		- [ ] Funktionalanalysis: abgebrochen
		- [ ] Opti 2: ab Oktober
		- [ ] Num 2 ???
		- [ ] Stochastik 2 ???
	- [ ] Master Vorlesungen
 - [ ] [Tims Karten](http://timbaumann.info/uni-spicker/) umsetzen
 - [ ] auf AnkiWeb SharedDecks Veröffentlichen anfangen
 - [ ] Englische Übersetzungen?


## Special Thanks
+ [Daniel A](https://gitlab.com/esclear) :ant: ([CSV Checker Skript](MMskripte/csv-checker.py))
+ Felix W :owl: (Early Testing)
+ [Ingo Blechschmidt](https://gitlab.com/iblech) :turtle: (Korrektur Deluxe, Motivation, ehem. Deckspeicher)
+ Julia G :bird: (Lineare Algebra I)
+ [Kilian R](https://gitlab.com/kiandru) :unicorn: (Readme-Layout-Spezialist, Blockchain Entrepreneur)
+ Leonie N :shark: (Motivation, Beratung) 
+ [Michael Struwe](https://gitlab.com/shak-mar) :frog: ([PDF Skripte](MMskripte), [CI](.gitlab-ci.yml), git Setup, [Licensing](LICENSE))
+ Moritz H :wolf: (Numerik I, [Mailing](http://supersecret.me/ankiprojekt/))
+ [Rainer E](https://gitlab.com/fanaticae) :leopard: (SQL Hacker, Python-Connaisseur, [APKG Skript](MMskripte), [CI](.gitlab-ci.yml), Ana3)
+ Roland M :dragon_face: (Korrektur Numerik I)
+ [Xaver H](https://gitlab.com/xaverdh) :dolphin: (1 Meister, [Docker](docker))


### Kontakt
**alex.m.s@gmx.de**


## Copyright Notice
    Mathe-Maister
    Copyright (C) 2019  Alexander Mai

    This information is free: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Additionally you have to include the URL to this project 
    <https://gitlab.com/CptMaister/Mathe-Maister/> conspicuously. If
    you are distributing the apkg-files of this project, you are further 
    required to either leave the first note untouched and unhidden from 
    the user, as it contains a link to this project. Or, if you want to
    change the content of the first note for any reasons such as for
    example for translation, you have to include the link to this project
    in a field of the note that is clearly readable when using it on
    Anki (the official desktop client, at least version 2.0) and when 
    using it on AnkiDroid or Anki Mobile either in an immediately visible 
    field or in an easy to see and click hint field.

    This work is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

