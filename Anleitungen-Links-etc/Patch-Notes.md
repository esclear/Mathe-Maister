# Geplante Updates:
 - Decks mit gewünschter Priorität
 - eigene Website
 - APKG Skript mit gewünschter Optionengruppe inklusive Leech Limit, Optionenname und tägliches Limit für Wiederholungsn und neue Karten
 - APKG Skript mit richtiger Erstellungszeit (Created) statt Unix 0 (eher unwichtig)
 - Gästebuch
 - neue Vorlesungen, wird bald gestartet: LA2, Opti1



# Patch Notes


### V **1.3.0 ← 1.2.X** - 19-07-XX -- *** ***
 - 
 - CSV-Checker zur besseren und leichteren Fehlerbehebung, vor allem für zukünftige Beitragende

### V **1.2.0 ← 1.1.22** - 19-06-12 -- ***Bilder jetzt auch im PDF (+Mathematiker, +Anleitung, +Copyright-Vorkehrungen)***
 - Bilder in den PDFs, mit neu erstelltem Docker für dieses Projekt :)
 - bei Karten mit Personennamen jetzt die Person mit Lebenszeit und Nationalität im Lesehinweis überall hinzugefügt
 - PDFs jetzt mit neuem Header mit Links zu diesem Projekt auf jeder Seite
 - Anleitung zum Erstellen von Inhalten überarbeitet (auch im README verlinkt)
 - Korrektur LA1, Auslagerung von gemeinsamem Ana1 und LA Stoff in neues Deck: Ana LA Grundlagen
 - neue Vorlesungen: Topologie, Funktionentheorie, Analysis 1, (abgebrochen: Funktionalanalysis)
 - Lizenzbedingungen überarbeitet im Readme

### V **1.1.0 ← 1.0.15** - 19-04-20 -- ***Bilder im Deck (+Lizenz, +schönere Karten)***
 - jetzt mit neuem Feld Illustration und das apkg-Skript bindet die Bilder auch passend in die apkg ein
 - neues Deck: Seminar Geometrie von Flächen
 - neues Deck: Seminar zu Differentialformen (pausiert)
 - Komma Style fix, mit '$,' zu ' ,$'
 - jetzt mit conditional fields in den card templates, damit nicht extra leere Zeilen dranstehen, wenn die optionalen Felder leer sind
 - jetzt mit Lizenz
 - Tags gefixt

### V **1.0.0 ← 0.8.10** - 19-03-09 -- ***Vollautomatisierung: APKG-Skript***
 - funktionierendes APKG Skript (paar Bugs, aber ok)
 - Ana3 und Geo Deck halb fertig, durch das APKG Skript jetzt auch immer auf dem neusten Stand benutzbar
 - in CSVs Tags und IDs aktualisiert eingeführt
 - keine APKG oder sonstigen Dateien mehr im Repo, ab jetzt nur noch Inhalte sammeln und hier und da Sachen anpassen, evtl Bugfixes und neue Features

### V **0.8.0 ← 0.7.12** - 19-02-12 -- ***Erste fertige Decks***
 - jetzt mit Patch Notes!
 - LA1 Sammlung fertig (bis auf Korrektur)
 - Numerik Sammlung vorerst fertig, schon korrigiert
 - CSVtoTEX jetzt um AWK Skript ergänzt, fehlt noch leere Zeilen weglassen
 - Style fix: Optionale Felder starten großgeschrieben und enden mit Punkt, wenn nicht im Mathe-Modus
 - Fehlen von "Lesehinweis" auf der Karten-Rückseite gefixt

### V **0.7.0** - 19-01-30 -- ***Usability-Tests***
 - Anki-Import Update Bedingungen fast komplett durchblickt
 - Anki-Decks ausgelagert (leicht verbuggt)
 - erstes veröffentlichtes Numerik 1 Deck
 - neues öffentliches LA1 Deck
 - PDF jetzt lesbarer formatiert, mehr Infos, Kommentarfunktionen
 - Umstrukturiert, Dokumente gesammelt, Readme ausgebaut

### V **0.6.0** - 19-01-22 -- ***README-Kunst***
 - Readme anspruchsvoller, mit Special Thanks und Emojis
 - erstes Merge Request
 - neue Latex Skills (u.a. \operatorname)
 - mehr LA1, Numerik und Ana3

### V **0.5.0** - 19-01-17 -- ***Automatisierung PDFs***
 - Skripte für CSV → TEX → PDF
 - Gitlab CI für Skripte
 - E-Mail-Verteiler für Updates

### V **0.4.0** - 18-12-19 -- ***Nicht mehr nur Tests***
 - erstes veröffentlichtes Anki-Deck (aktuelle LA1 Vorlesung auf aktuellem Stand)
 - readme gefüllt mit Erklärungen für Nutzer

### V **0.3.0** - 18-12-16 -- ***GitLab***
 - Wechsel github → gitlab

### V **0.2.0** - 18-12-06 -- ***Besseres Fundament***
 - Numerik Test Anki-Deck
 - Notiztypen kombiniert, angepasst
 - Ordner umstrukturiert
 - neue Latex Skills (u.a. \limits)
 - Dateien angelegt und erster Erstellungsplan

### V **0.1.0** - 18-11-11 -- ***Erste Deckstruktur***
 - erstes Test Anki-Deck (Geometrie)
 - Anki-Karten jetzt mit Latex durch Mathjax Skript
 - Notiztypen festgelegt und festgehalten

### V **0.0.0** - 18-10-20 -- ***Anfang***
 - angefangen
