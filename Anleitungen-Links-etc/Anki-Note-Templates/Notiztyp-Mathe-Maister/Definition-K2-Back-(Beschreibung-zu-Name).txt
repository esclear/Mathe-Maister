{{Beschreibung}}<br>
<br>

<span style="font-size: 15px;">
{{Typ}} {{Literaturhinweis}}<br>
{{Themenbereich}}<br>
{{Vorlesung}}</font-size> 
</span><br>

<hr id=answer>

{{Name}}<br><br>

{{hint:Formel}}<br>
{{hint:Interpretation}}<br>
{{hint:Beispiele}}<br>
{{hint:FunFacts}}<br>
{{hint:Trivia}}<br>
{{hint:Lesehinweis}}<br>
{{hint:Extras}}<br>
{{hint:Übersetzung}}



<script type="text/javascript">
(function() {
  if (window.MathJax != null) {
    var card = document.querySelector('.card');
    MathJax.Hub.Queue(['Typeset', MathJax.Hub, card]);
    return;
  }
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML';
  document.body.appendChild(script);
})();
</script>

<script type="text/x-mathjax-config">
MathJax.Hub.processSectionDelay = 0;
MathJax.Hub.Config({
  messageStyle: 'none',
  showProcessingMessages: false,
  tex2jax: {
    inlineMath: [['$', '$']],
    displayMath: [['$$', '$$']],
    processEscapes: true
  }
});
</script>