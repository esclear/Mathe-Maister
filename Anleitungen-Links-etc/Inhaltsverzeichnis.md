# Inhaltsverzeichnis  

1. [Grundlagenvorlesungen](#1-grundlagenvorlesungen)  
 1.0 [Ana LA Grundlagen](#10-ana-la-grundlagen) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.pdf))  
 1.1 [Lineare Algebra 1](#11-lineare-algebra-1) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.pdf))  
 1.2 [Lineare Algebra 2](#12-lineare-algebra-2) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-II.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-II.pdf))  
 1.3 [Analysis 1](#13-analysis-1) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.pdf))  
 1.4 [Analysis 2](#14-analysis-2)  
 1.5 [Einführung in die Algebra](#15-einführung-in-die-algebra)  
 1.6 [Einführung in die Geometrie](#16-einführung-in-die-geometrie) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.pdf))  
 1.7 [Optimierung 1](#17-optimierung-1)  
 1.8 [Numerik 1](#18-numerik-1) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.pdf))  
 1.9 [Stochastik 1 ???](#19-stochastik-1-???)  
2. [Weiterführende Vorlesungen](#2-weiterführende-vorlesungen)  
 2.1 [Analysis 3](#21-analysis-3) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.pdf))  
 2.2 [Algebra 2](#22-algebra-2)  
 2.3 [Funktionentheorie](#23-funktionentheorie) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.pdf))  
 2.4 [Funktionalanalysis](#24-funktionalanalysis) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Funktionalanalysis.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Funktionalanalysis.pdf))  
 2.5 [Topologie](#25-topologie) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.pdf))  
 2.6 [Optimierung 2](#26-optimierung-2)  
 2.7 [Numerik 2 ???](#27-numerik-2-???)  
 2.8 [Stochastik 2 ???](#28-stochastik-2-???)  
3. [Seminare](#3-seminare)  
 3.1 [Seminar Geometrie von Flächen](#31-seminar-geometrie-von-flächen) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flächen.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flächen.pdf))  
 3.2 [Pizzaseminar Differentialformen](#32-pizzaseminar-differentialformen) ([Anki](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.apkg) | [PDF](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.pdf))  
4. [Master Vorlesungen](#4-master-vorlesungen)  


## [1.](#inhaltsverzeichnis) Grundlagenvorlesungen  

### [1.0](#inhaltsverzeichnis) Ana LA Grundlagen    
 + WiSe + SoSe 18/19, Uni Augsburg, bei Prof Schneider und Prof Oldenburg  
 + Korrektur: Ingo  

Inhalte:  
 + Alle grundlegenden Inhalte der LA1 und Ana1 in einem Deck  

### [1.1](#inhaltsverzeichnis) Lineare Algebra 1  
 + WiSe 18/19, Uni Augsburg, bis ausschließlich Eigenwerte, bei Prof Schneider  
 + Autoren: Julia G, Alex M  
 + Korrektur: Ingo  

Sehr ausführliches Deck mit vielen FunFacts etc.  

Inhalte:
 - Mengen, Abbildungen, Relationen
 - Gruppen, Ringe, Körper
 - Vektorräume
 - Lineare Abbildungen
 - Determinanten


### [1.2](#inhaltsverzeichnis) Lineare Algebra 2 
 + läuft aktuell  
 + SoSe 19, Uni Augsburg, bei Prof Schneider  

### [1.3](#inhaltsverzeichnis) Analysis 1  
 + läuft aktuell  
 + SoSe 19, Uni Augsburg, bei Prof Oldenburg  

### [1.4](#inhaltsverzeichnis) Analysis 2  
 + voraussichtlich ab Oktober  

### [1.5](#inhaltsverzeichnis) Einführung in die Algebra  
 + vielleicht ab Oktober  

### [1.6](#inhaltsverzeichnis) Einführung in die Geometrie    
 + pausiert, demnächst gehts weiter  
 + SoSe 19, Uni Augsburg, bei Prof Cieliebak  

### [1.7](#inhaltsverzeichnis) Optimierung 1  
 + hoffentlich bald

### [1.8](#inhaltsverzeichnis) Numerik 1  
 + WiSe 18/19, Uni Augsburg, bis ausschließlich Interpolation durch Splines, bei Prof Peterseim  
 + Autoren: Alex M  
 + Korrektur: Roland M  

 Insgesamt kein extrem ausführliches Deck.

Inhalte:
 - Computerarithmetik
 - Skalare Gleichungen
 - Lineare Gleichungssysteme
 - Lineare Ausgleichsprobleme
 - Interpolation


### [1.9](#inhaltsverzeichnis) Stochastik 1 ???  



## [2.](#inhaltsverzeichnis) Weiterführende Vorlesungen  

### [2.1](#inhaltsverzeichnis) Analysis 3    
 + pausiert  
 + SoSe 19, Uni Augsburg, bei Prof Blömker  

### [2.2](#inhaltsverzeichnis) Algebra 2  
 + ab April 2020?  

### [2.3](#inhaltsverzeichnis) Funktionentheorie    
 + läuft aktuell  
 + SoSe 19, Uni Augsburg, bei Prof Hien  

### [2.4](#inhaltsverzeichnis) Funktionalanalysis    
 + abgebrochen  
 + SoSe 19, Uni Augsburg, bei Prof Schmidt  

### [2.5](#inhaltsverzeichnis) Topologie    
 + läuft heiß  
 + SoSe 19, Uni Augsburg, bei Prof Hanke  

### [2.6](#inhaltsverzeichnis) Optimierung 2  
 + ab Oktober  

### [2.7](#inhaltsverzeichnis) Numerik 2 ???  

### [2.8](#inhaltsverzeichnis) Stochastik 2 ???  



## [3.](#inhaltsverzeichnis) Seminare

### [3.1](#inhaltsverzeichnis) Seminar Geometrie von Flächen    
 + läuft aktuell  
 + SoSe 19, Uni Augsburg, bei Prof Cieliebak  


### [3.2](#inhaltsverzeichnis) Pizzaseminar Differentialformen    
 + bis auf weiteres pausiert  


## [4.](#inhaltsverzeichnis) Master Vorlesungen