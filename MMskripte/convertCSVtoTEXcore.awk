#!/bin/awk -E

# Separator for fields: TAB (\t)
BEGIN { FS = "\t" }

{

	# Ignore lines starting with "HEADER" (first line in my files), "Hallo Freund ..." (second line in my files) and comment lines (starting with "#")
	if ($1 ~ /^Hallo Freund/) {next}
	if ($1 ~ /^\#/) {next}

    name=$1
    description=$2
    xnotes=$3
    lecture=$4
    type=$5
    topic=$6
    formula=$7
    interpretation=$8
    funfacts=$9
    trivia=$10
    examples=$11
    readinghint=$12
    extras=$13
    literature=$14
    translation=$15
    xhintfront=$16
    xhintback=$17
    illustration= $18
    tags=$19
    id=$20
    
    # 2 empty lines before every entry
    print "\\\\\\\\"

    printf "\\\\\\textbf{%s} (%s", name, type
    if (literature) printf " %s", literature
    printf "; %s)", topic
    print "\\\\" description
	# optional fields
    if (illustration) printf "\\\\\\includesvg{Stoffsammlung/%s/%s}",substr(filename, 2, length(filename) - 2),substr(illustration, 11, length(illustration) - 18)
    if (formula) print "\\\\\\textbf{F:} " formula
    if (interpretation) print "\\\\\\textbf{I:} " interpretation
    if (funfacts) print "\\\\\\textbf{FF:} " funfacts
    if (trivia) print "\\\\\\textbf{T:} " trivia
    if (examples) print "\\\\\\textbf{e.g.:} " examples
    if (readinghint) print "\\\\\\textbf{Hint:} " readinghint
    if (extras) print "\\\\\\textbf{Extra:} " extras
    if (translation) print "\\\\\\textbf{Lang:} " translation

    # print STR # gibt STR aus, mit implizitem newline (aber irgendwie doch nicht)
    # printf FORMAT ARGS # gibt FORMAT (ohne implizites newline) aus, ersetzt Zeug mit ARGS
}
