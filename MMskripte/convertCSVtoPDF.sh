#!/bin/sh

#get absolute path
here=$(cd $(dirname $0); pwd)

# hook Überreste
#set -e

a=$1
# delete everything after "|" to not fix these HTML commands from your CSV to LATEX commands: &lt; &gt; &nbsp; <br/>. It is usually not necessary to delete them though, as not writing in HTML results in "fixHTMLinTEX" to do nothing
$here/convertCSVtoTEXAnkimathe.sh $a < $a | $here/fixHTMLinTEX.sh > ${a%.csv}.tex
latexmk -shell-escape -pdf ${a%.csv}.tex -interaction=nonstopmode

# how to run this script in my machine:
# bimmler@bimmler-Aspire-ES1-131 ~/Desktop/Ankimathe/ankiskript $ 
#./converttoPDF.sh LA1.csv
