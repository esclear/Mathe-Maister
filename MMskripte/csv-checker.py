#!/usr/bin/env python3

import sys

csv_path = sys.argv[1]

comment_count = 0

with open(csv_path) as csv_file:
	for line in csv_file:
		if line == '':
			continue

		# Ignore lines beginning with a #
		if line.startswith('#'):
			comment_count += 1
			continue

		# Split line at tabs
		name, desc, notes, lecture, card_type, topic, formula, interpretation, fun_facts, trivia, examples, hint, extras, literature, translation, front_hint, back_hint, illustration, tags, note_id, single_sided, priority = line.split('\t')

		# if line == 'exact match':
		# 	do stuff

		# if line.contains('substring'):
		# 	do stuff

		# if line.starswith('beginning'):
		#	do stuff

		# if hint == '':
		#	do stuff

		# if name.replace('$', '') == 'foo':
		#	do stuff

print('{} lines commented out'.format(comment_count))
