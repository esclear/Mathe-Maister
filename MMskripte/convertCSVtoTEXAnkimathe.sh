#!/bin/sh

# Hashtag zum kommentieren, aber erste Zeile ist kein Kommentar, sagt, dass sh verwendet werden soll, liegt in bin
# jetzt sagen, wo die .csv liegt, entweder reingeben, oder Pfad geben
# $1 hei�t, hier wird das 1. Argument genommen
## cat $1
# progA | progB gibt die Ausgabe von A als Eingabe in B rein, f�hrt B dann auch aus
# while f�r alle Zeilen, ; und ENTER ungef�hr gleich

here=$(cd $(dirname $0); pwd)

# Kopf des Latex-Dokuments
cat <<END_OF_FILE
\documentclass{article}

\usepackage{amsmath,amsthm,amssymb,amscd,stmaryrd,color,graphicx,environ,tabto,mathrsfs}
\usepackage[utf8]{inputenc}
\usepackage[margin=3cm]{geometry}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{svg}

\pagestyle{fancy}
\rfoot{Seite \thepage}
\chead{Korrekturlese-/Überblicksversion einer Vorlesung aus der Mathe-Maister-Stoffsammlung. Infos und mehr unter \href{https://gitlab.com/CptMaister/Mathe-Maister/}{\textbf{gitlab.com/CptMaister/Mathe-Maister}}}

\begin{document}

END_OF_FILE

# Verarbeitung der Inhalte der CSV-Datei
awk -v "filename=\"$(basename $1 .csv)\"" -f $here/convertCSVtoTEXcore.awk 

# Noch ein Anwendungsbeispiel f�r Here-Docs (Das mit <<BLA):
#   python3 <<END_OF_SCRIPT
#   print("Hello World!")
#   END_OF_SCRIPT

##exit # beendet das Skript hier

# Latex-Dokument schlie�en
cat <<END_OF_FILE
\\\\\\\\
\\textbf{Name} (Typ Literaturhinweis; Themenbereich) \\\\
Beschreibung \\\\
\\textbf{F}: Formel \\\\
\\textbf{I}: Interpretation \\\\
\\textbf{FF}: FunFacts \\\\
\\textbf{T}: Trivia, hier gibts meistens alternative Schreibweisen \\\\
\\textbf{e.g.}: Beispiele \\\\
\\textbf{Hint}: Lesehinweis \\\\
\\textbf{Extra}: Extras \\\\
\\textbf{Lang}: Übersetzung

\end{document}
END_OF_FILE

# cmd-Befehl f�r SKRIPT <(nimmt) CSV >(gibt aus) TEXDATEI
# /home/bimmler/Desktop/Ankimathe/ankiskript/fixHTMLinTEX.sh < /home/bimmler/Desktop/Ankimathe/ankiskript/convertCSVtoTEXAnkimathe.sh < /home/bimmler/Desktop/Ankimathe/ankiskript/LA1.csv > /home/bimmler/Desktop/Ankimathe/ankiskript/latextest.tex

