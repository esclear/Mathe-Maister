#!/bin/sh

#set -e

sed -e "s/&lt;/</g" -e "s/&gt;/>/g" -e 's/<br\/>/\\\\/g' -e 's/<br>/\\\\/g' -e "s/&nbsp;/~/g" -e 's/<b>/\\textbf\{/g' -e 's/<\/b>/\}/g' -e 's/<i>/\\textit\{/g' -e 's/<\/i>/\}/g' 
